import requests
from bs4 import BeautifulSoup as bs

#########################################
#### Web Crawler on MoneyControl.com ####
#########################################
print("All values are in Indian Ruppes (₹)")

# Global Variables
bse_list = []
nse_list = []
bse_prev_price_list = []
nse_prev_price_list = []
# URL of the page for crawler
page = requests.get('https://www.moneycontrol.com/india/stockpricequote/food-processing/krbl/KRB01')
soup = bs(page.content, 'html.parser')

# crawling using the class name of the <div>
stock_value = soup.find_all(class_='pcnsb div_live_price_wrap')
bse_value = stock_value[0]
nse_value = stock_value[1]

current_nse_price = nse_value.find_all('span')
current_bse_price = bse_value.find_all('span')
for bse_val, nse_val in zip(current_bse_price, current_nse_price):
    bse_list.append(bse_val.get_text())
    nse_list.append(nse_val.get_text())


print("BSE Value is : "+bse_list[0])
print("BSE Value is UP by : "+bse_list[2])
print("NSE Value is : "+nse_list[0])
print("NSE Value is UP by: "+nse_list[2])

more_data_prices = soup.find_all(class_='clearfix mkt_openclosebx')
bse_data_price = more_data_prices[0].find(class_='open_lhs1').find('ul').find_all('li')
nse_data_price = more_data_prices[1].find(class_='open_lhs1').find('ul').find_all('li')
for bse_price, nse_price in zip(bse_data_price, nse_data_price):
    bse_prev_price_list.append(bse_price.find_all('p')[1].get_text())
    nse_prev_price_list.append(nse_price.find_all('p')[1].get_text())

print("BSE Previous Close price is : "+bse_prev_price_list[0])
print("BSE Open price is : "+bse_prev_price_list[1])
print("NSE Previous Close price is : "+nse_prev_price_list[0])
print("NSE Open price is : "+nse_prev_price_list[1])
